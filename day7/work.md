
## 9.后台轮播图轮播图接口

- 本章是编写轮播图接口

本章目录

```js
.
├── package.json
├── src
│   ├── controller
│   │   ├── slider.ts
│   │   └── user.ts
│   ├── exceptions
│   │   └── HttpException.ts
│   ├── index.ts
│   ├── middlewares
│   │   └── errorMiddleware.ts
│   ├── models
│   │   ├── index.ts
│   │   ├── slider.ts
│   │   └── user.ts
│   ├── public
│   ├── typings
│   │   ├── express.d.ts
│   │   └── jwt.ts
│   └── utils
│       └── validator.ts
└── tsconfig.json
```

本章效果
![getslidersapi](https://images.gitee.com/uploads/images/2020/0623/224912_b210341c_1720749.png)

### 9.1 src\index.ts

src\index.ts

```diff
import express, { Express, Request, Response, NextFunction } from 'express';
import mongoose from 'mongoose';
import HttpException from './exceptions/HttpException';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';
import errorMiddleware from './middlewares/errorMiddleware';
import *  as userController from './controller/user';
+import *  as sliderController from './controller/slider';
import "dotenv/config";
import multer from 'multer';
import path from 'path';
+import { Slider } from './models';
const storage = multer.diskStorage({
    destination: path.join(__dirname, 'public', 'uploads'),
    filename(_req: Request, file: Express.Multer.File, cb) {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage });
const app: Express = express();
app.use(morgan("dev"));
app.use(cors());
app.use(helmet());
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.get('/', (_req: Request, res: Response) => {
    res.json({ success: true, message: 'hello world' });
});
app.get('/user/validate', userController.validate);
app.post('/user/register', userController.register);
app.post('/user/login', userController.login);
app.post('/user/uploadAvatar', upload.single('avatar'), userController.uploadAvatar);
+app.get('/slider/list', sliderController.list);
app.use((_req: Request, _res: Response, next: NextFunction) => {
    const error: HttpException = new HttpException(404, 'Route not found');
    next(error);
});
app.use(errorMiddleware);
const PORT: number = (process.env.PORT && parseInt(process.env.PORT)) || 8000;
(async function () {
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useUnifiedTopology', true);
    await mongoose.connect(process.env.MONGODB_URL!);
+    await createSliders();
    app.listen(PORT, () => {
        console.log(`Running on http://localhost:${PORT}`);
    });
})();

+async function createSliders() {
+    const sliders = await Slider.find();
+    if (sliders.length == 0) {
+        const sliders:any = [
+            { url: 'http://img.zhufengpeixun.cn/post_reactnative.png' },
+            { url: 'http://img.zhufengpeixun.cn/post_react.png' },
+            { url: 'http://img.zhufengpeixun.cn/post_vue.png' },
+            { url: 'http://img.zhufengpeixun.cn/post_wechat.png' },
+            { url: 'http://img.zhufengpeixun.cn/post_architect.jpg' }
+        ];
+        Slider.create(sliders);
+    }
+}
```

### 9.2 controller\slider.ts

src\controller\slider.ts

```js
import { Request, Response } from "express";
import { ISliderDocument, Slider } from "../models";
export const list = async (_req: Request, res: Response) => {
  let sliders: ISliderDocument[] = await Slider.find();
  res.json({ success: true, data: sliders });
};
```

### 9.3 models\slider.ts

src\models\slider.ts

```js
import mongoose, { Schema, Document } from "mongoose";
export interface ISliderDocument extends Document {
  url: string;
  _doc: ISliderDocument;
}
const SliderSchema: Schema<ISliderDocument> = new Schema(
  {
    url: String,
  },
  { timestamps: true }
);

export const Slider =
  mongoose.model < ISliderDocument > ("Slider", SliderSchema);
```

### 9.4 src\models\index.ts

src\models\index.ts

```diff
export * from './user';
+export * from './slider';
```

## 10.前台轮播图

- 本章实践前台轮播图
- 本章需要实践的内容
  - 使用`useRef`取得 DOM 元素
  - 使用 `antdesign` 的轮播图组件

本章目录

```js
.
├── package.json
├── src
│   ├── api
│   │   ├── home.tsx
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   ├── HomeHeader
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   └── HomeSliders
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Login
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   ├── Profile
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Register
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mime.tsx
│   │       └── profile.tsx
│   └── typings
│       ├── images.d.ts
│       ├── login-types.tsx
│       ├── slider.tsx
│       └── user.tsx
├── tsconfig.json
└── webpack.config.js
```

本章效果
![homesliders](https://images.gitee.com/uploads/images/2020/0623/224912_28eb9028_1720749.gif)

### 10.1 action-types.tsx

src\store\action-types.tsx

```diff
export const ADD = "ADD";
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = "SET_CURRENT_CATEGORY";
//发起验证用户是否登录的请求
export const VALIDATE = "VALIDATE";

export const LOGOUT = "LOGOUT";
//上传头像
export const CHANGE_AVATAR = "CHANGE_AVATAR";
+export const GET_SLIDERS = "GET_SLIDERS";
```

### 10.2 actions\home.tsx

src\store\actions\home.tsx

```diff
import * as TYPES from "../action-types";
+import { getSliders } from "@/api/home";
export default {
  setCurrentCategory(currentCategory: string) {
    return { type: TYPES.SET_CURRENT_CATEGORY, payload: currentCategory };
  },
+  getSliders() {
+    return {
+      type: TYPES.GET_SLIDERS,
+      payload: getSliders(),
+    };
+  }
};

```

### 10.3 typings\slider.tsx

src\typings\slider.tsx

```js
export default interface Slider {
  url: string;
}
```

### 10.4 reducers\home.tsx

src\store\reducers\home.tsx

```diff
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
+import Slider from "@/typings/slider";
export interface HomeState {
  currentCategory: string;
+  sliders: Slider[];
}
let initialState: HomeState = {
  currentCategory: "all", //默认当前的分类是显示全部类型的课程
+  sliders: [],
};
export default function (
  state: HomeState = initialState,
  action: AnyAction
): HomeState {
  switch (action.type) {
    case TYPES.SET_CURRENT_CATEGORY: //修改当前分类
      return { ...state, currentCategory: action.payload };
+    case TYPES.GET_SLIDERS:
+      return { ...state, sliders: action.payload.data };
    default:
      return state;
  }
}
```

### 10.5 api\home.tsx

src\api\home.tsx

```js
import axios from "./index";
export function getSliders() {
  return axios.get("/slider/list");
}
```

### 10.6 HomeSliders\index.tsx

src\routes\Home\components\HomeSliders\index.tsx

```js
import React, { PropsWithChildren, useRef, useEffect } from "react";
import { Carousel } from "antd";
import "./index.less";
import Slider from "@/typings/slider";
type Props = PropsWithChildren<{
  children?: any,
  sliders?: Slider[],
  getSliders?: any,
}>;
function HomeSliders(props: Props) {
  useEffect(() => {
    if (props.sliders.length == 0) {
      props.getSliders();
    }
  }, []);
  return (
    <Carousel effect="scrollx" autoplay>
      {props.sliders.map((item: Slider, index: number) => (
        <div key={index}>
          <img src={item.url} />
        </div>
      ))}
    </Carousel>
  );
}

export default HomeSliders;
```

### 10.7 HomeSliders\index.less

src\routes\Home\components\HomeSliders\index.less

```less
.ant-carousel .slick-slide {
  text-align: center;
  height: 320px;
  line-height: 320px;
  background: #364d79;
  overflow: hidden;
}

.ant-carousel .slick-slide {
  color: #fff;
  img {
    width: 100%;
    height: 320px;
  }
}
```

### 10.8 routes\Home\index.tsx

src\routes\Home\index.tsx

```diff
import React, { PropsWithChildren, useRef } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import actions from "@/store/actions/home";
import HomeHeader from "./components/HomeHeader";
import { CombinedState } from "@/store/reducers";
import { HomeState } from "@/store/reducers/home";
+import HomeSliders from "./components/HomeSliders";
import "./index.less";
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params {}
type Props = PropsWithChildren<
  RouteComponentProps<Params> & StateProps & DispatchProps
>;
function Home(props: Props) {
+  const homeContainerRef = useRef(null);
  return (
    <>
      <HomeHeader
        currentCategory={props.currentCategory}
        setCurrentCategory={props.setCurrentCategory}
      />
+      <div className="home-container" ref={homeContainerRef}>
+        <HomeSliders sliders={props.sliders} getSliders={props.getSliders} />
+      </div>
    </>
  );
}
let mapStateToProps = (state: CombinedState): HomeState => state.home;
export default connect(mapStateToProps, actions)(Home);

```
