let a:number=10;
let zname:string ;
let arr:number[]=[1,2,3];
let arr2:Array<number>  =[];
//写项目的实在掉不定,直接 用any
let anyValue:any = 1;

//接口 行为的抽象和对象的形状
interface Speakable{
    name:string;
    speak():void;
}

let speakman:Speakable ={
    name:'zhufeng',
    speak(){

    }
}
//泛型  是指在定义函数\接口\类的时候,不预先指定具体的类型,而是使用的时候再指定类型的一种特性
function createArray<T>(length:number,value:T):Array<T>{
   let result:Array<T> =[];
   for(let i=0;i<length;i++){
       result[i]= value;
   }
   return result;
}
let v1:Array<string> =createArray<string>(3,'a');
console.log(v1);

// keyof 索引类型的查询操作符
interface Person{
    name:string;
    age:number;
}
//PersonKey = name | age;
type PersonKey = keyof Person;

function getValueByKey(p:Person,key:PersonKey){

}
let p:Person = {name:'',age:10};
//getValueByKey(p,'xx');

//returnType
function getUserInfo(){
    return {name:'zhufeng'};
}
type UserType = ReturnType<typeof getUserInfo>;
let u:UserType={name:'xx',10}
//类型声明 类型声明可以让我们


let value = {name:'zhufeng'};
type  V = typeof value;
